# Procedura creazione utenti NethServer #

## 0. Setup iniziale ##

Assicurarsi che `python3` sia installato.

```bash
# yum install -y python3
```

## 1. Esportazione utenti Gsuite ##

Esportare gli utenti dal pannello di amministrazione Gsuite.

## 2. Creare i relativi file CSV ##

Chiamare lo script python per generare i file richiesti da NethServer:

```bash
$ python3 gsuite_export_parser.py User_Download_[TIMESTAMP].csv
```

Il risultanto sono i seguenti file CSV:

### users.csv ###

Esempio:

```csv
Nome utente senza dominio,Nome e cognome,Password iniziale casuale
```

### emails.csv ###

Esempio:

```csv
Nome utente senza dominio,Indirizzo email
```

## 3. Importare gli utenti in NethServer ##

Esecuzione degli script:

```bash
$ /usr/share/doc/nethserver-sssd-`rpm --query --qf "%{VERSION}" nethserver-sssd`/scripts/import_users users.csv ','
```

```bash
$ /usr/share/doc/nethserver-sssd-`rpm --query --qf "%{VERSION}" nethserver-sssd`/scripts/import_emails emails.csv ','
```

Tratto da: https://docs.nethserver.org/en/v7/accounts.html

## 3a. Creare i gruppi utente (opzionale) ##

Per agevolare l'eliminazione dei diplomati si può creare un gruppo per anno scolastico
e un gruppo per i docenti in questo modo:

```csv
anno,utente1,utente2,utente3,...
...
...
...
docenti,utente1,utente2,utente3,...
```

Importare successivamente i gruppi con il comando:

```bash
$ /usr/share/doc/nethserver-sssd-`rpm --query --qf "%{VERSION}" nethserver-sssd`/scripts/import_groups groups.csv ','
```

## 4. Fornire le password agli utenti ##

Tramite registro elettronico o altro canale comunicare agli utenti le proprie password iniziali
con istruzioni per cambiarle all'URL: https://registro.carducci-galilei.it/

## 5. Sincronizzazione con utenti GSuite ##

La sincronizzazione avverrà automaticamente ogni 15 minuti.

## Extra ##

Gli utenti possono essere eliminati usando un file CSV:

```csv
utente1
utente2
...
docente1
```

e usando il comando (controlla il nome del file in fondo):

```bash
$ usr/share/doc/nethserver-sssd-`rpm --query --qf "%{VERSION}" nethserver-sssd`/scripts/delete_users delete.csv
```
