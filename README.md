# Install guide #

## 0. Setup ##

Install `python3`:

```bash
# yum install -y python3
```

## 1. Add module to cockpit ##

```bash
$ bash install.sh
```

## 2. Change domain ##
Change the DOMAIN variable in `bin/parser`:

```python
DOMAIN="CHANGEME"
```

## 3. Change privileged groups (optional) ##
Add required groups to `authorizedGroups` in `js/script.js`:

```javascript
let authorizedGroups = ['root'];
```
