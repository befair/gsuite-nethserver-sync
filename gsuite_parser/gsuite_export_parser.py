#!/usr/bin/python3

import csv
import sys

USERS_CSV = "users.csv"


# Check params
if not len(sys.argv) > 1:
    print(f"USE: {sys.argv[0]} [GSUITE CSV EXPORT FILE]")
    exit(100)

counter = 0

with open(sys.argv[1], newline='') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', quotechar='"')

    # Create the 'users.csv' file
    ufile = open(USERS_CSV, 'w', newline='')
    user_writer = csv.writer(ufile, delimiter=',', quotechar='"')

    for user in reader:
        if counter != 0:
            username = user[2].split('@')[0]
            fullname = f"{user[0].capitalize()} {user[1].capitalize()}"
            groups = user[5][1:]

            # Write to users.csv
            user_writer.writerow([username, fullname, "PASSWORD", groups])

        counter += 1

    ufile.close()
    print(f"Exported {counter-1} users ({USERS_CSV})")
