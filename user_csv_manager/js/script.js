const BIN_PATH = "/usr/share/cockpit/user_csv_manager/bin/"
const LOG_FILE = "/var/log/user_csv_manager/manager.log"
const TMP_FILE = "/tmp/users_tmp.csv"

// Groups authorized to use this plugin
let authorizedGroups = ['root'];

// File reader
let fr;

// Check if user is authorized
checkUserAuth().then(res => {
  if(res == false)
    cockpit.logout();
});


// Start reading log
read_log();

/* Import users from CSV */
function importUsers() {
    let file = $("#import_file").prop("files")[0];

    // Check if file is a CSV
    if(file === undefined || file['name'].split('.')[1] != "csv") {
        alert("Selezione un file CSV valido!");
        return;
    }

    // Write the received CSV
    fr = new FileReader();
    fr.readAsText(file);
    fr.onload = importCallback;

    alert("Procedura iniziata.\nControlla il log per sapere lo stato.");
}

/* Import callback */
function importCallback() {
    // Save source CSV
    cockpit.file(TMP_FILE).replace(fr.result);

    // Parse CSV and start importing
    cockpit.spawn(["/usr/bin/python3", BIN_PATH + "parser", TMP_FILE]).then((result) => {
        if(result) {
          alert("Le seguenti righe non sono valide. Correggi gli errori e riprova:\n" + result);
        }
        else {
          // Start import process
          cockpit.script([BIN_PATH + "import_users_from_csv"]);
        }
    });
}

/* Delete users from CSV */
function deleteUsers() {
    let file = $("#delete_file").prop("files")[0];

    // Check if file is a CSV
    if(file === undefined || file['name'].split('.')[1] != "csv") {
        alert("Selezione un file CSV valido!");
        return;
    }

    // Write the received CSV
    fr = new FileReader();
    fr.readAsText(file);
    fr.onload = deleteCallback;

    alert("Procedura iniziata.\nControlla il log per sapere lo stato.");
}

/* Delete callback */
function deleteCallback() {
    // Save source CSV
    cockpit.file(TMP_FILE).replace(fr.result);

    // Start import process
    cockpit.script([BIN_PATH + "delete_users_from_csv"]);
}

/* Logger */
function read_log() {
    // Clean log
    cockpit.file(LOG_FILE).replace("");

    // Start watching log file
    cockpit.file(LOG_FILE).watch((content, tag, error) => {
        document.getElementById("log").value = content;
    });
}

function checkUserAuth() {
  return cockpit.user().then(user => {
    let rv = false;

    authorizedGroups.forEach(group => {
      if(user['groups'].indexOf(group) >= 0)
        rv = true;
    });

    return rv;
  })
}
