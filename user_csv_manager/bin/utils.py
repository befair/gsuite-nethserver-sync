import csv
import os
import json
import re
import subprocess
from time import time

BACKUP_PATH = "/var/backups/"

def validate_csv(filename):
    faulty_lines = []

    with open(filename, newline='') as f:
        reader = csv.reader(f, delimiter=',', quotechar='"')
        # Regex for password check
        # At least:
        # 1 Uppercase
        # 1 Special character
        # 1 Digit
        # Length >= 8
        password_check = re.compile("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$")

        for i, line in enumerate(reader, 1):
            # Check if line is valid
            if len(line) != 4:
                faulty_lines.append((i, line))
                continue

            # Check if password respects NethServer policy
            if not password_check.match(line[2]):
                faulty_lines.append((i, line))
                continue

            # Check if password contains non-ascii characters
            if not all(ord(c) < 128 for c in line[2]):
                faulty_lines.append((i, line))
                continue

            # Each line must have at least 1 group
            if line[3] == "":
                faulty_lines.append((i, line))
                continue

    return faulty_lines

def get_nethserver_groups():
    # Retrive current groups and users
    groups = json.loads((subprocess.check_output(["/usr/bin/sudo", "/usr/libexec/nethserver/list-groups"])))
    for group in groups:
        members = json.loads((subprocess.check_output(["/usr/bin/sudo", "/usr/libexec/nethserver/list-group-members", group])))
        groups[group]['members'] = [m.split('@')[0] for m in members]

    # Backup current groups with timestamp
    if not os.path.isdir(BACKUP_PATH):
        os.mkdir(BACKUP_PATH)

    ts = str(time()).split('.')[0]
    with open(f"{BACKUP_PATH}/groups_backup_{ts}.json", "w") as f:
        f.write(json.dumps(groups))

    return groups


def prepare_ldap_query_file(path, users_map):
    with open(path, "w") as f:
        # LDAP configuration stuff
        f.write('declare -A ldapconf\n')
        f.write('exec </etc/nethserver/ldappasswd.conf\n')
        f.write('while read -r pkey pval; do\n')
        f.write('if [[ ${pkey} != [A-Z]* ]]; then\n')
        f.write('continue\n')
        f.write('fi\n')
        f.write('ldapconf[${pkey}]="${pval}"\n')
        f.write('done\n')
        f.write('export LDAPBASE="${LDAPBASE:-${ldapconf[BASE]}}"\n')
        f.write('export LDAPBINDDN="${LDAPBINDDN:-uid=${USER%%@${ldapconf[DOMAIN]}},${ldapconf[BASE]}}"\n')
        f.write('export LDAPURI="${LDAPURI:-${ldapconf[URI]}}"\n')

        # Loop through users
        for user in users_map:
            f.write('ldapmodify -D "administrator@carducci-galilei.it" -W -x -y <(echo -n "ProvaBeFair123#") << EOF\n')
            f.write('dn: CN=%s,CN=Users,DC=ad,DC=carducci-galilei,DC=it\n' % user['username'])
            f.write('changetype: modify\n')
            f.write('replace: comment\n')
            f.write('comment: $(echo -n \'%s\' | sha1sum | awk \'{print $1}\')\n' % user['password'])
            f.write('EOF\n')

            f.write('ldapmodify -D "administrator@carducci-galilei.it" -W -x -y <(echo -n "ProvaBeFair123#") << EOF\n')
            f.write('dn: CN=%s,CN=Users,DC=ad,DC=carducci-galilei,DC=it\n' % user['username'])
            f.write('changetype: modify\n')
            f.write('replace: description\n')
            f.write('description: %s\n' % user['groups'])
            f.write('EOF\n')
