# Copy new version
rm -rf /usr/share/cockpit/user_csv_manager
cp -r user_csv_manager /usr/share/cockpit/user_csv_manager

# Create work stuff
mkdir -p /var/log/user_csv_manager
mkdir -p /var/backups/
touch /var/log/user_csv_manager/manager.log

# Change permissions
chmod 777 /var/log/user_csv_manager
chmod 777 /var/log/user_csv_manager/manager.log
chmod 777 /var/backups/

# Copy sudoers file
cp 90_nethserver_user_manager.sudoers /etc/sudoers.d/90_nethserver_user_manager
