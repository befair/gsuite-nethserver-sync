# Custom script #

## change-user-passwd ##

Set the new user password in LDAP (used later for sync in Gsuite)

## character-validation ##

Prevent user from using non-ascii characters (forbidden in Gsuite but not in NethServer)
