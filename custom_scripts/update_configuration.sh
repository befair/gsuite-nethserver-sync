# Password change interceptor
cp /usr/libexec/nethserver/api/system-password/change-user-passwd /usr/libexec/nethserver/api/system-password/change-user-passwd.bck
cp scripts/change-user-passwd /usr/libexec/nethserver/api/system-password/

# Password ascii check
cp scripts/character-validation /etc/e-smith/validators/actions/
ln -s /etc/e-smith/validators/actions/character-validation /etc/e-smith/validators/password-strength/S20character-validation
